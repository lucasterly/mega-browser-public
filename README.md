# simple-browser

# Run local:

## Get the files
clone the repo or download

## Production version
```
git clone https://github.com/lucasterly/simple-browser.git
```
## Last version
```
git clone https://lucasterly@bitbucket.org/lucasterly/mega-browser-public.git
```
## go to the file
```
cd simple-browser
```
## To create a virtual environment in the current directory, execute the following command:
```
python3 -m venv venv
```
## This creates the venv/ folder. To activate the virtual environment on Windows, run:
```
call venv/scripts/activate.bat
```
## MacOS or Linux
```
source venv/bin/activate
```
## You can see that the virtual environment is active by the (venv) prefix in your shell
## To now install PyQ5 and PyQtWebEngine issue the following command:
```
pip install PyQt5
pip install PyQtWebEngine
```
to run the browser rename the file main.28193013.py to main.py
and run the browser with
```
python3 main.py
```